<?php
//addlashes

$str = "Is your name O'Reilly?";

// Outputs: Is your name O\'Reilly?
echo addslashes($str);
//explode
$str = 'html,css,php';
$arr = explode(',',$str);
print_r($arr);
echo "<br>";
//implode
$webcoach = array('html','css','php');
$tutorial = implode(',',$webcoach);
var_dump($tutorial);
echo "<br>";
//html entities
$line_with_html_tag= "this is <br> tag";
echo htmlentities($line_with_html_tag);
echo "<br>";
$html_tags= "<html>
<head>
<title></title>
</head>
<body>      body structure </body>
</html>";
echo "<pre>";
echo htmlentities($html_tags);
echo "</pre>";
echo "<br>";
//trim(front space remove korbe)
echo trim($html_tags);
echo "<br>";
//substr_compare
echo substr_compare("abcde","bc",1,2);

echo "<br>";
echo substr_compare("abcde","de",-2,2);

echo "<br>";
echo substr_compare("abcde","cd",1,2);
echo "<br>";
echo substr_compare("abcde","abc",5,1);
echo "<br>";
echo substr_compare("abcde","bcg",1,2);
echo "<br>";
//substr_count
